package com.zare.loop

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextInput
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.zare.loop.view.screen.activity.SignUpActivity
import com.zare.loop.view.screen.activity.SimpleUI
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ExampleUITest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun searchScreen_displaysCorrectlyWithResults() {
        composeTestRule.setContent {
            SimpleUI()
        }
        composeTestRule.onNodeWithTag("TextField").performTextInput("Hello, World!")

        // Find the Button and click it
        composeTestRule.onNodeWithTag("Button").performClick()

        // Verify that the Text is displayed with the correct content
        composeTestRule.onNodeWithTag("DisplayText")
            .assertIsDisplayed()
            .assertTextEquals("You entered: Hello, World!")
    }
}