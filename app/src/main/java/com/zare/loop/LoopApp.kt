package com.zare.loop

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class LoopApp : Application(){
}