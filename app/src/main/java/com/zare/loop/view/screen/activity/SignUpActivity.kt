package com.zare.loop.view.screen.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.zare.loop.R
import com.zare.loop.util.extenstionFragment.addFragment
import com.zare.loop.view.screen.fragment.SignUpFragment

class SignUpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addFragment(
            R.id.fragmentContainer,
            SignUpFragment.newInstance()
        )
    }
}