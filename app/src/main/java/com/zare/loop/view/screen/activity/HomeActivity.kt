package com.zare.loop.view.screen.activity

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.zare.loop.R
import com.zare.loop.data.model.Movie
import com.zare.loop.databinding.ActivityDetialsBinding
import com.zare.loop.view.screen.fragment.HomeFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

     private lateinit var binding: ActivityDetialsBinding

    private var data: Movie? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetialsBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }


    fun setMovie(movie: Movie) {
        this.data = movie
    }

    fun getMovie(): Movie? {
        return data
    }

}