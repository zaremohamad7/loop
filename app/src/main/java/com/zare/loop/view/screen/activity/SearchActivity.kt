package com.zare.loop.view.screen.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.sharp.Star
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.rememberNavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.zare.loop.R
import com.zare.loop.util.convertDateToYear
import com.zare.loop.util.roundToDecimalPlaces
import com.zare.loop.viewModels.ArchiveMoveiViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchActivity : AppCompatActivity() {

    private val archiveMoveiViewModel: ArchiveMoveiViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Surface(
                modifier = Modifier.fillMaxSize(),
                color = MaterialTheme.colorScheme.background
            ) {
                SimpleUI()
//                SearchScreen(archiveMoveiViewModel)
            }

        }
    }
}
@Composable
fun SimpleUI() {
    var text by remember { mutableStateOf(TextFieldValue("")) }
    var displayText by remember { mutableStateOf("") }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.Center
    ) {
        TextField(
            value = text,
            onValueChange = { text = it },
            label = { Text("Enter text") },
            modifier = Modifier
                .fillMaxWidth()
                .testTag("TextField")
        )
        Spacer(modifier = Modifier.height(16.dp))
        Button(
            onClick = { displayText = text.text },
            modifier = Modifier
                .fillMaxWidth()
                .testTag("Button")
        ) {
            Text("Submit")
        }
        Spacer(modifier = Modifier.height(16.dp))
        if (displayText.isNotEmpty()) {
            Text(
                text = "You entered: $displayText",
                modifier = Modifier
                    .fillMaxWidth()
                    .testTag("DisplayText")
            )
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBar(onBackClick: () -> Unit, title: String) {
    CenterAlignedTopAppBar(
        title = {
            Text(
                text = "All Movies",
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        },
        navigationIcon = {
            onBackClick?.let { navigationUpAction ->
                IconButton(onClick = { navigationUpAction() }) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "Back"
                    )
                }
            }
        },
        modifier = Modifier.fillMaxWidth()
    )
}

@Composable
fun SearchScreen(archiveMoveiViewModel: ArchiveMoveiViewModel = viewModel()) {
    val context = LocalContext.current

    val searchText = remember { mutableStateOf("") }

    archiveMoveiViewModel.selectArchiveMove()

    val searchResults by archiveMoveiViewModel.archiveMovieResult.collectAsState(initial = emptyList())

    val filteredResults = if (searchText.value.isNotEmpty()) {
        searchResults.filter { movie ->
            movie.name.contains(searchText.value, ignoreCase = true)
        }
    } else {
        searchResults
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        horizontalAlignment = Alignment.CenterHorizontally
    )
    {
        TopBar(onBackClick = {
            context.startActivity(Intent(context, HomeActivity::class.java))
            (context as? ComponentActivity)?.finish()
        }, title = "")

        TextField(
            modifier = Modifier
                .wrapContentSize()
                .padding(8.dp)
                .clip(RoundedCornerShape(10.dp))
                .border(2.dp, Color.White, RoundedCornerShape(10.dp))
                .background(Color.White, MaterialTheme.shapes.medium)
                .clip(MaterialTheme.shapes.medium),
            value = searchText.value,
            onValueChange = { searchText.value = it },
            leadingIcon = {
                Icon(imageVector = Icons.Default.Search, contentDescription = null)
            },
            label = { Text("Search") },
            colors = OutlinedTextFieldDefaults.colors(
                focusedContainerColor = Color.White,
                unfocusedContainerColor = Color.White,
                disabledContainerColor = Color.White
            ),
        )

        LazyColumn {
            items(filteredResults.size) { index ->
                val result = filteredResults[index]
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically

                ) {
                    AsyncImage(
                        model = ImageRequest.Builder(LocalContext.current)
                            .data(result.img)  
                            .crossfade(true)
                            .build(),
                        placeholder = painterResource(R.drawable.ic_launcher_background),
                        contentDescription = null,
                        modifier = Modifier
                            .clip(MaterialTheme.shapes.medium)
                            .height(89.dp)
                            .width(60.dp),  
                        contentScale = ContentScale.Crop
                    )

                    Spacer(modifier = Modifier.width(8.dp))

                    Column(
                        modifier = Modifier
                            .weight(1f)
                            .fillMaxWidth(),
                        verticalArrangement = Arrangement.Center
                    ) {

                        Text(
                            text = convertDateToYear(result.year),
                            textAlign = TextAlign.Center
                        )
                        Text(
                            text = result.name,
                            textAlign = TextAlign.Center,
                            fontWeight = FontWeight.Bold
                        )
                        RatingStar(rating = roundToDecimalPlaces(result.rating.toDouble()).toFloat())
                    }

                    Spacer(modifier = Modifier.width(8.dp))
                    Image(
                        painter = painterResource(id = R.drawable.favorite),  
                        contentDescription = null,
                        modifier = Modifier
                            .size(19.dp)  
                    )
                }
            }
        }
    }

}

@Composable
fun RatingStar(rating: Float) {
    val fullStars = rating.toInt()
    val fractionalStar = rating - fullStars

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(2.dp),
        horizontalArrangement = Arrangement.Start
    ) {
        for (i in 1..5) {
            val isFilled = if (i <= fullStars) true else i - fullStars <= fractionalStar
            val image = if (isFilled) Icons.Default.Star else Icons.Sharp.Star
            val tint = if (isFilled) Color(0xFFFFA500) else Color.Gray

            Icon(
                imageVector = image,
                contentDescription = "Rating Star",
                tint = tint,
                modifier = Modifier
                    .size(24.dp)
                    .padding(4.dp)
            )
        }
    }
}
