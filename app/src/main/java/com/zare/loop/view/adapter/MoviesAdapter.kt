package com.zare.loop.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zare.loop.R
import com.zare.loop.data.model.Movie

class MoviesAdapter(private val listener: OnItemClickListener) :
    RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    private var dataList: List<Movie> = emptyList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_movies, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = dataList[position]
        val imageUrl = movie.posterUrl
        Glide.with(holder.itemView.context)
            .load(imageUrl)
            .centerCrop()
            .into(holder.imageView)


    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun setData(newData: List<Movie>) {
        dataList = newData
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView: ImageView = itemView.findViewById(R.id.imageViewMovies)
        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    listener.onItemClick(dataList[position])
                }
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(movie: Movie)
    }
}
