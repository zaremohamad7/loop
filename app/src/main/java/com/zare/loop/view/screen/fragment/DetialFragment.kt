package com.zare.loop.view.screen.fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.zare.loop.R
import com.zare.loop.data.model.Movie
import com.zare.loop.databinding.FragmentSecondBinding
import com.zare.loop.view.screen.activity.HomeActivity
import com.zare.loop.util.convertDateToFormattedString
import com.zare.loop.util.convertDateToYear
import com.zare.loop.util.convertDateToYearWithP
import com.zare.loop.util.convertLanguageName
import com.zare.loop.util.convertRuntimeToString
import com.zare.loop.util.formatBudget
import com.zare.loop.util.formatMovieDetails
import com.zare.loop.util.formatRevenue
import com.zare.loop.util.roundToDecimalPlaces
import com.zare.loop.view.adapter.GenreAdapter

class DetialFragment : Fragment() {

    private lateinit var activity: HomeActivity

    private var movieData: Movie? = null


    private var _binding: FragmentSecondBinding? = null

    private val binding get() = _binding!!

    private lateinit var genersAdapter: GenreAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        val view = binding.root

        receiveData()
        setupMovieRecyclerView(view)

        binding.closeImageView.setOnClickListener {
            findNavController().popBackStack()
        }
        return view


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        movieData?.let { bindMovieData(it) }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as HomeActivity
    }

    private fun receiveData() {
        movieData = activity.getMovie()
    }

    private fun generesData(genresList: List<String>) {
        genersAdapter.setData(genresList)
    }

    private fun setupMovieRecyclerView(view: View) {
        val recyclerViewMovie: RecyclerView = view.findViewById(R.id.genersRecyclerView)
        genersAdapter = GenreAdapter()

        recyclerViewMovie.adapter = genersAdapter
        recyclerViewMovie.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

    }
    private fun bindMovieData(movieData: Movie) {
        binding.yearTextView.text = convertDateToFormattedString(movieData.releaseDate)
        binding.movieDurationTextView.text = movieData.runtime?.let { convertRuntimeToString(it) }
        binding.movieNameTextView.text = movieData.title
        binding.yearMovieNameTextView.text = convertDateToYearWithP(movieData.releaseDate)
        binding.movieOverviewTextView.text = movieData.overview
        Glide.with(this)
            .load(movieData.posterUrl)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.ic_launcher_foreground)
            )
            .transition(DrawableTransitionOptions.withCrossFade()) 
            .into(binding.asyncImage)

        generesData(movieData.genres)

        binding.revenueTextView.text = formatRevenue(movieData.revenue)
        binding.budgetTextView.text = formatBudget(movieData.budget)
        binding.movieRatingSecond.rating = roundToDecimalPlaces(movieData.rating).toFloat()
        binding.orgiLanTextView.text = convertLanguageName(movieData.language)
        binding.ratingTextView.text =  formatMovieDetails(movieData)

    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}

