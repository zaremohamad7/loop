package com.zare.loop.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zare.loop.R
import com.zare.loop.data.model.Movie

class FilterAdapter(private val listener: FilterAdapter.OnItemClickListener) :
    RecyclerView.Adapter<FilterAdapter.ViewHolder>() {

    private var dataList: List<String> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_filter, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val genre = dataList[position]
        holder.nameText.text = genre

    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun setData(newData: List<String>) {
        dataList = newData
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameText: TextView = itemView.findViewById(R.id.filterTextView)

        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val genre = dataList[position]
                    listener.onItemClickFilter(genre)
                }
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClickFilter(gener: String)
    }
}