package com.zare.loop.view.screen.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.button.MaterialButton
import com.zare.loop.R
import com.zare.loop.view.screen.activity.HomeActivity


class SignUpFragment : Fragment() {

    private lateinit var signUpButton: MaterialButton

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        signUpButton = view.findViewById(R.id.signUpButton)

        setup()
    }
    companion object {
        @JvmStatic
        fun newInstance() = SignUpFragment()
    }
    private fun setup() {
        setupClickListeners()
    }
    private fun setupClickListeners() {
        signUpButton.setOnClickListener {
            startActivity(Intent(requireContext(), HomeActivity::class.java))
        }
    }
}