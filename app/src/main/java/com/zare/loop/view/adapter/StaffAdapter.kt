package com.zare.loop.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zare.loop.R
import com.zare.loop.data.model.Staff
import com.zare.loop.util.convertDateToYear
import com.zare.loop.util.roundToDecimalPlaces

class StaffAdapter(private val listener: OnItemClickListenerStaff) :
    RecyclerView.Adapter<StaffAdapter.ViewHolder>() {


    private var dataList: List<Staff> = emptyList()
    private var filteredDataList: List<Staff> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_staff, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val staff = filteredDataList[position]
        val imageUrl = staff.posterUrl
        val year = staff.releaseDate
        val title = staff.title

        holder.yearText.text = convertDateToYear(year)
        holder.nameText.text = title
        holder._movieRating.rating = roundToDecimalPlaces(staff.rating).toFloat()

        Glide.with(holder.itemView.context)
            .load(imageUrl)
            .centerCrop()
            .into(holder.imageView)
    }

    override fun getItemCount(): Int {
        return filteredDataList.size
    }

    fun setData(newData: List<Staff>) {
        dataList = newData
        filteredDataList = newData
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView: ImageView = itemView.findViewById(R.id.staffImageView)
        val yearText: TextView = itemView.findViewById(R.id.movieYearTextView)
        val nameText: TextView = itemView.findViewById(R.id.movieNameTextView)
        val _imageFavorite: ImageView = itemView.findViewById(R.id.imageFavorite)
        val _movieRating: RatingBar = itemView.findViewById(R.id.movieRating)

        init {

            _imageFavorite.setOnClickListener {
                _imageFavorite.setImageResource(R.drawable.favorite_2)
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val staff = dataList[position]
                    listener.onItemClickStaff(staff)
                }
            }
        }
    }

    fun setFilteredData(genre: String) {
        filteredDataList = if (genre.isEmpty()) {
            dataList
        } else {
            dataList.filter { it.genres.contains(genre) }
        }
        notifyDataSetChanged()
    }

    interface OnItemClickListenerStaff {
        fun onItemClickStaff(movie: Staff)
    }
}
