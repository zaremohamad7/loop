package com.zare.loop.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zare.loop.R


class GenreAdapter : RecyclerView.Adapter<GenreAdapter.ViewHolder>() {

    private var dataList: List<String> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_geners, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val staff = dataList[position]
        holder.nameText.text = staff

    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun setData(newData: List<String>) {
        dataList = newData
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameText: TextView = itemView.findViewById(R.id.genresTextView)
    }
}
