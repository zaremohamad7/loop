package com.zare.loop.view.screen.fragment

import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zare.loop.data.common.Result
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zare.loop.R
import com.zare.loop.data.model.ArchiveMovieData
import com.zare.loop.data.model.Movie
import com.zare.loop.data.model.Staff
import com.zare.loop.databinding.FragmentFirstBinding
import com.zare.loop.util.GenreUtil
import com.zare.loop.view.screen.activity.HomeActivity
import com.zare.loop.view.screen.activity.SearchActivity
import com.zare.loop.view.adapter.FilterAdapter
import com.zare.loop.view.adapter.MoviesAdapter
import com.zare.loop.view.adapter.StaffAdapter
import com.zare.loop.viewModels.ArchiveMoveiViewModel
import com.zare.loop.viewModels.MovieViewModel
import com.zare.loop.viewModels.StaffViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment : Fragment(), StaffAdapter.OnItemClickListenerStaff,
    MoviesAdapter.OnItemClickListener, FilterAdapter.OnItemClickListener {

    private lateinit var activity: HomeActivity

    private var _binding: FragmentFirstBinding? = null

    private val binding get() = _binding!!


    private val viewModelMoive: MovieViewModel by viewModels()

    private val viewModelStaff: StaffViewModel by viewModels()

    private val archiveMoveiViewModel: ArchiveMoveiViewModel by viewModels()


    private lateinit var moviesAdapter: MoviesAdapter

    private lateinit var staffAdapter: StaffAdapter

    private lateinit var filterAdapter: FilterAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        val view = binding.root

        setupRecyclerViews(view)
        setupCallingApi()
        setupValueResponse()

        binding.searchIcon.setOnClickListener {
            navigateToActivity()
        }

        return view

    }

    private fun navigateToActivity() {
        val intent = Intent(requireActivity(), SearchActivity::class.java)
        startActivity(intent)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as HomeActivity
    }

    private fun sendMovie(_movie: Movie) {
        activity.setMovie(_movie)
    }

    private fun setupRecyclerViews(view: View) {
        setupMovieRecyclerView(view)
        setupStaffRecyclerView(view)
        setupFilterRecyclerView(view)
    }

    private fun setupMovieRecyclerView(view: View) {

        val recyclerViewMovie: RecyclerView = view.findViewById(R.id.movieRecyclerView)
        moviesAdapter = MoviesAdapter(this)

        recyclerViewMovie.adapter = moviesAdapter
        recyclerViewMovie.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
    }

    private fun setupFilterRecyclerView(view: View) {
        val recyclerViewMovie: RecyclerView = view.findViewById(R.id.filterecyclerView)
        filterAdapter = FilterAdapter(this)

        recyclerViewMovie.adapter = filterAdapter
        recyclerViewMovie.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        setGenre()
    }

    private fun setupStaffRecyclerView(view: View) {
        val recyclerViewStaff: RecyclerView = view.findViewById(R.id.staffRecyclerView)
        staffAdapter = StaffAdapter(this)
        recyclerViewStaff.adapter = staffAdapter
        recyclerViewStaff.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun setupCallingApi() {
        viewModelMoive.fetchMovies()
        viewModelStaff.fetchStaff()
    }

    private fun setupValueResponse() {
        viewLifecycleOwner.lifecycleScope.launch {
            try {
                combine(
                    viewModelMoive.movies,
                    viewModelStaff.staff
                ) { movieResult, staffResult ->
                    Pair(movieResult, staffResult)
                }.collect { (movieResult, staffResult) ->
                    when (movieResult) {
                        is Result.Success -> {
                            val movies = movieResult.response
                            moviesAdapter.setData(movies)
                        }

                        is Result.Error -> {
                            Log.e(TAG, "Error loading movies: check the error")
                        }

                        is Result.Loading -> {
                            //TODO
                        }
                    }

                    when (staffResult) {
                        is Result.Success -> {
                            val staffs = staffResult.response
                            staffAdapter.setData(staffs)
                        }

                        is Result.Error -> {
                            Log.e(TAG, "Error loading movies: check the error")
                        }

                        is Result.Loading -> {
                            //TODO
                        }
                    }
                }
            } catch (e: Exception) {
                Log.e(TAG, "Error collecting data: ${e.message}")
            }
        }
    }


    private fun setGenre() {
        filterAdapter.setData(GenreUtil.getDefaultGenres())
    }


    override fun onItemClick(_movie: Movie) {
        sendMovie(_movie)
        findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
    }

    override fun onItemClickStaff(_staff: Staff) {

        val archiveMovieData = ArchiveMovieData(
            movieName = _staff.title,
            year = _staff.releaseDate,
            img = _staff.posterUrl,
            rating = _staff.rating
        )
        archiveMoveiViewModel.insertArchiveMovie(archiveMovieData)
    }

    override fun onItemClickFilter(genre: String) {
        staffAdapter.setFilteredData(genre)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}