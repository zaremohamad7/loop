package com.zare.loop.data.model

data class Staff(
    val rating: Double,
    val id: Int,
    val revenue: Int,
    val releaseDate: String,
    val director: Director,
    val posterUrl: String,
    val cast: List<Cast>,
    val runtime: Int,
    val title: String,
    val overview: String,
    val reviews: Int,
    val budget: Int,
    val language: String,
    val genres: List<String>
)
