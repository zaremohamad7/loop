package com.zare.loop.data.repositories.remote

import com.zare.loop.data.datasource.remote.ApiService
import com.zare.loop.data.model.Movie
import com.zare.loop.data.model.Staff
import javax.inject.Inject


class MovieRepository @Inject constructor(private val movieApi: ApiService) {

    suspend fun getMovies(): List<Movie> {
        return movieApi.getMovies()
    }

    suspend fun getStaff():  List<Staff> {
        return movieApi.getStaff()
    }



}
