package com.zare.loop.data.model

data class ArchiveMovieData(
    val movieName: String,
    val year: String,
    val img: String,
    val rating: Double
)