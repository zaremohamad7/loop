package com.zare.loop.data.datasource.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.zare.loop.data.model.ArchiveMovieData
import com.zare.loop.data.model.Staff

@Dao
interface ArchiveMovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertArchiveMovie(archiveMovieEntity: ArchiveMovieEntity)

    @Query("SELECT * FROM movies ORDER BY id DESC")
    suspend fun findArchiveMovie():List<ArchiveMovieEntity>
}