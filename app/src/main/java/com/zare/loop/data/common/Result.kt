package com.zare.loop.data.common


sealed class Result<out T> {
    data class Success<out T>(val response: T) : Result<T>()
    data class Error(val remoteSourceException: RemoteSourceException) : Result<Nothing>()
    data object Loading : Result<Nothing>()
}


