package com.zare.loop.data.datasource.remote

import com.zare.loop.data.model.Movie
import com.zare.loop.data.model.Staff
import retrofit2.http.GET

interface ApiService {

    @GET("movies.json")
    suspend fun getMovies(): List<Movie>

    @GET("staff_picks.json")
    suspend fun getStaff(): List<Staff>
}