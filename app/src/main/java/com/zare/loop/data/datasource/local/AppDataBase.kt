package com.zare.loop.data.datasource.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [ArchiveMovieEntity::class],
    version = 1
)
abstract class AppDataBase : RoomDatabase() {

    abstract fun getArchiveDao(): ArchiveMovieDao

    companion object {
        private var dbINSTANCE:AppDataBase?=null

        fun getAppDB(context: Context): AppDataBase {
            if (dbINSTANCE==null) {
                dbINSTANCE = Room.databaseBuilder<AppDataBase>(context.applicationContext,
                    AppDataBase::class.java, "my_database").allowMainThreadQueries()
                    .build()
            }
            return dbINSTANCE!!
        }
    }
}
