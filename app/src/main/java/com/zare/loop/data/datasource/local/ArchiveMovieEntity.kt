package com.zare.loop.data.datasource.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "movies")
data class ArchiveMovieEntity(
    var name: String, var year: String,
    var img: String,
    var rating : Double,
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null
)
