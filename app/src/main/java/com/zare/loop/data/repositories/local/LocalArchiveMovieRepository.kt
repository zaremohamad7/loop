package com.zare.loop.data.repositories.local


import com.zare.loop.data.datasource.local.ArchiveMovieDao
import com.zare.loop.data.datasource.local.ArchiveMovieEntity
import com.zare.loop.data.model.ArchiveMovieData
import com.zare.loop.data.model.Staff
import javax.inject.Inject

class LocalArchiveMovieRepository @Inject constructor(private val archiveApi: ArchiveMovieDao) {

    suspend fun insertArchiveMovie(archiveMovieData: ArchiveMovieData) {
        val movieInsert = ArchiveMovieEntity(archiveMovieData.movieName, archiveMovieData.year, archiveMovieData.img, archiveMovieData.rating)
        archiveApi.insertArchiveMovie(movieInsert)
    }
    suspend fun searchArchiveMovie():List<ArchiveMovieEntity> {
        return archiveApi.findArchiveMovie()
    }

}