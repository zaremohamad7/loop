package com.zare.loop.data.model

import android.os.Parcel
import android.os.Parcelable

data class Movie(
    val rating: Double,
    val id: Int,
    val revenue: Long,
    val releaseDate: String,
    val director: Director,
    val posterUrl: String,
    val cast: List<Actor>,
    val runtime: Int,
    val title: String,
    val overview: String,
    val reviews: Int,
    val budget: Long,
    val language: String,
    val genres: List<String>
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readDouble(),
        parcel.readInt(),
        parcel.readLong(),
        parcel.readString() ?: "",
        parcel.readParcelable(Director::class.java.classLoader)!!,
        parcel.readString() ?: "",
        parcel.createTypedArrayList(Actor)!!,
        parcel.readInt(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.readLong(),
        parcel.readString() ?: "",
        parcel.createStringArrayList()!!
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(rating)
        parcel.writeInt(id)
        parcel.writeLong(revenue)
        parcel.writeString(releaseDate)
        parcel.writeParcelable(director, flags)
        parcel.writeString(posterUrl)
        parcel.writeTypedList(cast)
        parcel.writeInt(runtime)
        parcel.writeString(title)
        parcel.writeString(overview)
        parcel.writeInt(reviews)
        parcel.writeLong(budget)
        parcel.writeString(language)
        parcel.writeStringList(genres)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Movie> {
        override fun createFromParcel(parcel: Parcel): Movie {
            return Movie(parcel)
        }

        override fun newArray(size: Int): Array<Movie?> {
            return arrayOfNulls(size)
        }
    }
}