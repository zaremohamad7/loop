package com.zare.loop.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.zare.loop.data.model.Movie
import com.zare.loop.data.repositories.remote.MovieRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject
import com.zare.loop.data.common.Result
import kotlinx.coroutines.delay

@HiltViewModel
class MovieViewModel @Inject constructor(private val movieRepository: MovieRepository) : ViewModel() {

     private val _movies = MutableStateFlow<Result<List<Movie>>>(Result.Loading)
    val movies: StateFlow<Result<List<Movie>>> get() = _movies

     fun fetchMovies() {
        viewModelScope.launch {
            try {
                val moviesList = movieRepository.getMovies()
                _movies.value = Result.Success(moviesList)
            } catch (e: Exception) {
            }
        }
    }
}
