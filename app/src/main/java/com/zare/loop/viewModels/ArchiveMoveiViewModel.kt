package com.zare.loop.viewModels


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.zare.loop.data.datasource.local.ArchiveMovieEntity
import com.zare.loop.data.model.ArchiveMovieData
import com.zare.loop.data.model.Staff
import com.zare.loop.data.repositories.local.LocalArchiveMovieRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ArchiveMoveiViewModel @Inject constructor( private val localArchiveMovieRepository: LocalArchiveMovieRepository) :
      ViewModel (){

    private val _archiveMovieResult = MutableStateFlow<List<ArchiveMovieEntity>>(emptyList())
    val archiveMovieResult: StateFlow<List<ArchiveMovieEntity>> get() = _archiveMovieResult

    fun insertArchiveMovie(archiveMovieData: ArchiveMovieData) {
        viewModelScope.launch {
            localArchiveMovieRepository.insertArchiveMovie(archiveMovieData)
        }
    }

    fun selectArchiveMove() {
        viewModelScope.launch {
            val result = localArchiveMovieRepository.searchArchiveMovie()
            _archiveMovieResult.value = result
        }
    }
}