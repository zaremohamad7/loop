package com.zare.loop.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.zare.loop.data.model.Staff
import com.zare.loop.data.repositories.remote.MovieRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject
import com.zare.loop.data.common.Result
import kotlinx.coroutines.Dispatchers

@HiltViewModel
class StaffViewModel @Inject constructor(private val movieRepository: MovieRepository) :
    ViewModel() {

    private val _staff = MutableStateFlow<Result<List<Staff>>>(Result.Loading)
    val staff: StateFlow<Result<List<Staff>>> get() = _staff

    fun fetchStaff() {
        viewModelScope.launch{
            try {
                val _staffList = movieRepository.getStaff()
                _staff.value = Result.Success(_staffList)
            } catch (e: Exception) {
            }
        }
    }
}