package com.zare.loop.util

object GenreUtil {
    fun getDefaultGenres(): List<String> {
        return listOf("Crime", "War", "Drama", "Action", "Thriller", "History", "Mystery")
    }
}