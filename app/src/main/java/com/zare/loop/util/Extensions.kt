package com.zare.loop.util


import com.zare.loop.data.model.Movie
import java.text.SimpleDateFormat
import java.util.Locale

fun convertRuntimeToString(runtimeInMinutes: Int): String {
    val hours = runtimeInMinutes / 60
    val remainingMinutes = runtimeInMinutes % 60
    val seconds = remainingMinutes * 60

    val formattedString = buildString {
        if (hours > 0) {
            append("$hours h ")
        }
        if (remainingMinutes > 0) {
            append("$remainingMinutes m ")
        }

    }

    return formattedString.trim()
}
fun formatRevenue(revenue: Long?): String {
    if (revenue == null) {
        return ""
    }

    return when {
        revenue >= 1000000 -> {
            val millions = revenue / 1000000
            val thousands = revenue % 1000000 / 1000
            "$$millions.${String.format("%03d", thousands)}"
        }
        revenue >= 1000 -> {
            val thousands = revenue / 1000
            "$$thousands K"
        }
        else -> "$$revenue"
    }
}
fun formatBudget(budget: Long?): String {
    if (budget == null) {
        return ""
    }

    return when {
        budget >= 1000000 -> {
            val millions = budget / 1000000
            val thousands = budget % 1000000 / 1000
            "$$millions.${String.format("%03d", thousands)}"
        }
        budget >= 1000 -> {
            val thousands = budget / 1000
            "$$thousands K"
        }
        else -> "$$budget"
    }
}
fun convertLanguageName(code: String?): String {
    return when (code!!.toLowerCase()) {
        "en" -> "English"
        else -> "Unknown"
    }
}
fun roundToDecimalPlaces(number: Double): Double {
    return String.format("%.${3}f", number).toDouble()
}


fun convertDateToYear(dateString: String): String {
    val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val outputFormat = SimpleDateFormat("yyyy", Locale.getDefault())

    return try {
        val date = inputFormat.parse(dateString)
        outputFormat.format(date!!)
    } catch (e: Exception) {
        "Invalid Date"
    }
}

fun convertDateToFormattedString(dateString: String): String {
    val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val outputFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())

    return try {
        val date = inputFormat.parse(dateString)
        outputFormat.format(date!!)
    } catch (e: Exception) {
        "Invalid Date"
    }
}
fun formatMovieDetails(movieData: Movie): String {
    val releaseYear = convertDateToYear(movieData.releaseDate)
    return "${roundToDecimalPlaces(movieData.rating)} ($releaseYear)"
}

fun convertDateToYearWithP(dateString: String): String {
    val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val outputFormat = SimpleDateFormat("yyyy", Locale.getDefault())

    return try {
        val date = inputFormat.parse(dateString)
        val year = outputFormat.format(date!!)
        "($year)"
    } catch (e: Exception) {
        "Invalid Date"
    }
}

